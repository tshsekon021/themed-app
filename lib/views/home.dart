import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:funky_app/views/profile_page.dart';
import 'package:funky_app/views/widgets/header_widgets.dart';
import 'package:funky_app/views/widgets/registration_page.dart';
import 'package:funky_app/views/widgets/theme_helper.dart';

import 'forgot_password.dart';

class FunkyHomeView extends StatefulWidget {
  const FunkyHomeView({Key? key, required this.title}): super(key: key);

  final String title;

  @override
  State<FunkyHomeView> createState() => _FunkyHomeViewState();
}

class _FunkyHomeViewState extends State<FunkyHomeView> {
  final double _headerHeight = 250;
  final Key _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: _headerHeight,
              child: HeaderWidget(_headerHeight, true, Icons.login_outlined),
            ),
            SafeArea(
                child: SizedBox(
                  child: Column(
                    children:  [
                      const Text(
                        'Hello',
                        style: TextStyle(fontSize: 60, fontWeight: FontWeight.bold),
                      ),
                      const Text(
                      'SignIn into your account',
                      style: TextStyle(color: Colors.blueGrey),
                      ),
                      const SizedBox(height: 30.0
                      ),
                      Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                decoration: ThemeHelper().inputBoxDecorationShadow(),
                                child: TextField(
                                  decoration: ThemeHelper().textInputDecoration('User Name', 'Enter your user name'),
                                ),
                              ),
                              const SizedBox(height: 30.0),
                              Container(
                                decoration: ThemeHelper().inputBoxDecorationShadow(),
                                child: TextField(
                                  obscureText: true,
                                  decoration: ThemeHelper().textInputDecoration('Password', 'Enter your password'),
                                ),
                              ),
                              const SizedBox(height: 15.0),
                              Container(
                                margin: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                                alignment: Alignment.topRight,
                                child: SizedBox(
                                  height: 50,
                                  child: GestureDetector(
                                    onTap: (){
                                      //navigate to register page
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> const ForgotPassword()),);
                                    },
                                    child: const Text('Forgot your password?',
                                      style: TextStyle(
                                        color: Colors.grey
                                      ) ,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                decoration: ThemeHelper().buttonBoxDecoration(context),
                                child: ElevatedButton(
                                  style: ThemeHelper().buttonStyle(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                                    child: Text('Sign In'.toUpperCase(),
                                    style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                                  ),
                                  onPressed: (){
                                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> const ProfilePage()));
                                  },
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    const TextSpan(text: "Don't have an account? "),
                                    TextSpan(text: "Create",
                                    recognizer: TapGestureRecognizer()
                                    ..onTap = (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> const RegistrationPage()));

                                    },
                                      style: TextStyle(fontWeight: FontWeight.bold, color: Theme.of(context).colorScheme.secondary)
                                    )
                                  ]
                                )


                              )
                            ],
                          )
                      )
                    ],
                  ),
                ))

          ],
        ),
      ),


    );
  }
}
