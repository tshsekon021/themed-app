import 'package:flutter/material.dart';
import 'package:funky_app/views/widgets/header_widgets.dart';

class SettingsPage extends StatefulWidget{
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage>{

  final double _headerHeight = 150;
  final _displayMode = ["Light", "Dark", "Contrast"];
  String ? _selectedValue = "Light";
  String ? options;
  bool _latestNotification = false;

  @override
  Widget build(BuildContext context){

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children:<Widget> [
            const SizedBox(
              height: 16.0,
            ),
            _buildToggleRow(context, 'Get the latest updates', _latestNotification, (value) => null),


            SizedBox(
              height: _headerHeight,
              child: HeaderWidget(
                _headerHeight, true, Icons.settings
              ),

            ),
            
            RadioListTile(
              title: const Text('Make profile private'),
              value: 'Make profile private',
              groupValue: options ,
              onChanged: (value){
                setState(() {
                  options = value.toString();
                });
              },
            ),

            RadioListTile(
              title: const Text('Make profile unlisted'),
              value: 'Make profile unlisted',
              groupValue: options ,
              onChanged: (value){
                setState(() {
                  options = value.toString();
                });
              },
            ),

            RadioListTile(
              title: const Text('Make profile public'),
              value: 'Make profile public',
              groupValue: options ,
              onChanged: (value){
                setState(() {
                  options = value.toString();
                });
              },
            ),

            DropdownButton(
              value: _selectedValue,
              items: _displayMode.map((e){
              return DropdownMenuItem(value: e,child: Text(e),);
            }
            ).toList(),
              onChanged: (value){
                setState(() {
                  _selectedValue = value as String;
                });

              },
              icon: const Icon(
                Icons.arrow_drop_down_circle,
                color: Colors.lightBlueAccent,
              ),
            ),
          ],
        ),

      ),

    );


  }
  _getNotification(bool value){
    if (_latestNotification == false){
      setState(() {
        _latestNotification = true;
      });
    }else{
      _latestNotification = false;
    }
  }

  Widget _buildToggleRow(BuildContext context, String label, bool initialValue, Function( bool value) updateFunction){
    return SwitchListTile.adaptive(title: Text(label),
    value: initialValue,
    onChanged: (updateFunction),
    );
  }
}