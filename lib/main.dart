import 'package:flutter/material.dart';
import 'package:funky_app/views/home.dart';

void main() {
  runApp(const FunkyApp());
}

class FunkyApp extends StatelessWidget {
  const FunkyApp({Key? key}): super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Funky App',
      theme: ThemeData(
        primaryColor: Colors.cyan,
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.cyanAccent),
        scaffoldBackgroundColor: Colors.grey.shade100,
        primarySwatch: Colors.grey
      ),
      home: const FunkyHomeView(title: 'Make me Nice'),
    );
  }
}